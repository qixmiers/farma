
var Noticias = function(){

	var cont = 1;
	var formfoto = $( '.form-fotos .form-init' ).html();

	return{

		init : function(){

			if (jQuery().datepicker) {
	            $('.date-picker').datepicker({
	                rtl: Metronic.isRTL(),
	                autoclose: true
	            });

	        }
			var handleWysihtml5 = function () {
		        if (!jQuery().wysihtml5) {
		            return;
		        }

		        if ($('.wysihtml5').size() > 0) {
		            $('.wysihtml5').wysihtml5({
		                "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
		            });
		        }
		    }

			handleWysihtml5();

			$( '#add-foto' ).on( 'click', Noticias.addfotos );

			 $('.select2_category').select2({
		            placeholder: "Selecione uma categoria",
		            allowClear: true
		        });

			$( 'a.foto-excluir' ).on( 'click', function(){
				var o = this;
				var cont = $(o).attr('data-id');
				console.log($(o).text());
				$( '#img-'+cont ).css( 'border', 'solid 1px red' );

			});

			$( '#form-noticia' ).submit(function(event){Noticias.cadastrar(event);});
		},
		addfotos : function(){

				cont++;
				var form = formfoto;

				$( '.form-fotos' ).append('<div class="col-md-3 foto-'+cont+'">'+form+'</div>');

				$( '.foto-'+cont+' p.title-foto' ).html( 'Foto' );

				$( '.foto-'+cont+' input[type="file"]' ).attr( 'name', 'nMedia'+cont );

				$( '.foto-'+cont+' input[type="text"]' ).attr( 'name', 'nFonte'+cont );

		},
		cadastrar : function(event){

			var nCategoria,
				nLayout,
				veri = false,
				nTitulo,
				nSubTitulo,
				nInicio,
				nCapa,
				nDescricao,
				msgerror = '',
				nLayout = '';

			//nLayout = $( '#opt-layout span.checked input' ).val();
			nCategoria = $( '.select2_category' ).select2('val');
			nTitulo = $( '#nTitulo' );
			nSubTitulo = $( '#nSubtitulo' );
			nInicio = $( '#nInicio' );
			nCapa = $( '#nCapa' );
			nDescricao = $( '.wysihtml5' ).val();
			nLayout = $( '#opt-layout span.checked input' );

			if(nTitulo.val() == ''){
				msgerror = 'Titulo é obrigatório.</br>';
				veri = true;
			}

			if(nSubTitulo.val() == ''){
				msgerror += 'SubTitulo é obrigatório.</br>';
				veri = true;
			}

			if(nInicio.val() == ''){
				msgerror += 'Data Início é obrigatório.</br>';
				veri = true;
			}

			if(nLayout == ''){
				msgerror += 'Tipo de layout é obrigatório.</br>';
				veri = true;
			}

			if(nCategoria == ''){
				msgerror += 'Categorio é obrigatório.</br>';
				veri = true;
			}

			if(nDescricao == ''){
				msgerror += 'Descriçao é obrigatório.</br>';
				veri = true;
			}


			if(nCapa.val() == '' && parseInt(nCapa.attr( 'data-edit' )) == 1){
				msgerror += 'Foto de capa é obrigatório.</br>';
				veri = true;
			}

			var body = $("html, body");
			body.animate({scrollTop:0}, '500', 'swing');

			if(veri == true){
				$( '.alert-erros' ).html( '<div class="Metronic-alerts alert alert-danger fade in" id="prefix_670267542417">'+
					'<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>'+
					'<i class="fa-lg fa fa-warning"></i>Error<br>'+msgerror+'</div>');
				event.preventDefault();
			}
			else{
				$( '#bg-noticia' ).show();
				return;
			}
		}
	}
}();
