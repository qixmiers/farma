var Perfil = function(){
	
	return{
		
		init : function(){
			
			$( '#btnSalvarConta' ).on( 'click', Perfil.dados );
			$( '#btnTrocarSenha' ).on( 'click', Perfil.alterarsenha );
			
			$( '#form-novousuario' ).submit(function(event){Perfil.novouser(event)});
			$( '#form-alterarusuario' ).submit(function(event){Perfil.alteraruser(event)});
		},
		dados : function(){
			
			$.getJSON('query/perfil.php',
					{
					'action' : 'perfil',
					'metodo' : 'alterar_conta',
					'sNome' : $( '#sNome' ).val(),
					'sEmail' :$( '#sEmail' ).val(),
					'sApelido' : $( '#sApelido' ).val()
					},
					function(resposta){
						
						if(resposta == 1){
							
							$( '.status-conta' ).html( 'Salvo!' );
							return;
						}
						
						$( '.status-conta' ).html( 'Nao foi possível salvar!' );
						
					}
					
			);
			
		},
		alterarsenha : function(){
			
			var sAtual, sNova, sNova2;
			
			sAtual = $( '#senhaatual' ).val();
			sNova = $( '#novasenha' ).val();
			sNova2 = $( '#novasenha2' ).val();
			
			if(sNova === sNova2){
				
				$.getJSON( 'query/perfil.php',
						{
							'action' : 'perfil',
							'metodo' : 'alterar_senha',
							'sAtual' : sAtual,
							'sNova' : sNova,
							'sNova2' : sNova2
						},
						function(resposta){
							
							if(resposta == 1){
								
								$( '.status-senha' ).html( 'Alterado com sucesso!' );
								$( '#senhaatual' ).val('');
								$( '#novasenha' ).val('');
								$( '#novasenha2' ).val('');  
								return;
							}else
								if(resposta == 0){
									$( '.status-senha' ).html( 'Nao foi possível salvar!' );
									return;
							}
							
						 	$( '.status-senha' ).html( resposta );
						}
				);
			}else{
				$( '.status-senha' ).html( 'Senhas diferentes!' );
			}
			
		},
		novouser : function(event){
			
			var sNome, sUsername, sEmail, sSenha, erro = false;
			
			sNome = $( 'input[name="uNomeCompleto"]' );
			sUsername = $( 'input[name="uApelido"]' );
			sEmail = $( 'input[name="uEmail"]' );
			sSenha = $( 'input[name="uSenha"]' );
			
			if ( sNome.val() == '' ){
				
				erro = true;
				sNome.attr( 'style', 'border:1px solid red' );
			}
			else{
				sNome.attr( 'style', false );
			}
			
			if ( sUsername.val() == '' ){
				
				erro = true;
				sUsername.attr( 'style', 'border:1px solid red' );
			}
			else{
				sUsername.attr( 'style', false );
			}
			
			if ( sEmail.val() == '' ){
				
				erro = true;
				sEmail.attr( 'style', 'border:1px solid red' );
			}
			else{
				sEmail.attr( 'style', false );
			}
			
			if ( sSenha.val() == '' ){
				
				erro = true;
				sSenha.attr( 'style', 'border:1px solid red' );
			}
			else{
				sSenha.attr( 'style', false );
			}
			
			if(erro == false)
				return;
			else
				event.preventDefault();
			
		},
		alteraruser : function(event){
			
			var sNome, sUsername, sEmail, erro = false;
			
			sNome = $( 'input[name="uNomeCompleto"]' );
			sUsername = $( 'input[name="uApelido"]' );
			sEmail = $( 'input[name="uEmail"]' );
			
			
			if ( sNome.val() == '' ){
				
				erro = true;
				sNome.attr( 'style', 'border:1px solid red' );
			}
			else{
				sNome.attr( 'style', false );
			}
			
			if ( sUsername.val() == '' ){
				
				erro = true;
				sUsername.attr( 'style', 'border:1px solid red' );
			}
			else{
				sUsername.attr( 'style', false );
			}
			
			if ( sEmail.val() == '' ){
				
				erro = true;
				sEmail.attr( 'style', 'border:1px solid red' );
			}
			else{
				sEmail.attr( 'style', false );
			}
			
			
			if(erro == false)
				return;
			else
				event.preventDefault();
			
		},
		modal : function(acao,key){
			
			if(acao == 1){
				
				$( '#portlet-config .modal-title' ).html( 'Deletar usuario' );
				$( '#portlet-config .modal-body' ).html( 'Tem certeza que deseja deletar?' );
				$( '#portlet-config #btn-acao' ).attr('href','perfil.php?key='+key+'&acao=deletar').removeClass( 'blue' ).addClass( 'red' ).html( 'Deletar' );
			}
			
			$( '#portlet-config' ).modal( 'show' );
		}
		
	};
}();