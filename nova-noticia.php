<?php
session_start();

require 'vendor/autoload.php';
use Aws\S3\S3Client;

use model\datamapper\NoticiaMapper;
use model\datamapper\MediaMapper;
use model\activerecord\NoticiaModel;
use model\activerecord\MediaModel;
use model\datamapper\Db;

$s3 = S3Client::factory(array(
		'key'    => 'AKIAIAZDPX7F4S2TY75Q',
		'secret' => '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',
		'region' => 'sa-east-1'
));

require_once 'autoload.php';

$config = parse_ini_file(__DIR__ . DIRECTORY_SEPARATOR . 'db.ini');

$db = Db::getInstance($config);

$dmNoticia = new NoticiaMapper($db);

$model = new NoticiaModel();
$model->setDm($dmNoticia);
$model->setUserId($_SESSION['id']);

$dmMedia = new MediaMapper($db);

$media = new MediaModel();
$media->setDm($dmMedia);
$bucket = 'consulfarmanoticia/medias';
$avatar = 'http://www.cubomob.com.br/consulfarmanoticias/img/foto_capa_b.png';
//ini_set('display_errors', true);
$nDataInicio = date( 'd/m/Y' );
$dataedit = 1;
if($_GET['ID']){

	$model->setNoticiaId((int)$_GET['ID']);
	$noticia = $model->dados();

	$media->setMediaId($_GET['ID']);
	$noticiaMedia = $media->dados();

	$nTitulo = $noticia->getTitulo();
	$nSubTitulo = $noticia->getSubTitulo();
	$nDataInicio = $model->getdataBR($noticia->getDataInserido());
	$nFonte = $noticia->getFonte();

	if($noticia->getDataExpira() != '0000-00-00 00:00:00')
		$nDataExpira = $model->getdataBR($noticia->getDataExpira());
	$avatar = 'https://s3-sa-east-1.amazonaws.com/consulfarmanoticia/medias/'.$noticia->getUrlAvatar();
	$nDescricao = $noticia->getDescricao();

	if($noticiaMedia[0]){
		//if(function_exists($noticiaMedia[0]->getTipoMedia())){
			if($noticiaMedia[0]->getTipoMedia() == 1){
				$nVideo = $noticiaMedia[0]->getUrlMedia();
				$nVideoId = $noticiaMedia[0]->getMediaId();
			}
		//}
	}
	$action = '?ID='.$_GET['ID'].'&'.'update=true';
	$categoriaId = $noticia->getCategoriaId();
	$dataedit = 2;
}

if($_POST){


	$model->setTitulo($_POST['nTitulo']);
	$model->setSubTitulo($_POST['nSubtitulo']);
	$model->setDataInserido($model->getdata($_POST['nInicio']));
	$model->setDataExpira($model->getdata($_POST['nTermino']));
	$model->setCategoriaId($_POST['nCategoria']);
	$model->setTipoNoticia($_POST['nLayout']);
	$model->setDescricao($_POST['nDescricao']);
	$model->setFonte($_POST['nFonte']);

	if($_POST['nNotificar'] == 2)
		$model->nf = true;

	if($_FILES["nCapa"]["name"]){
		$fileName = $_FILES["nCapa"]["name"];
		$fileTmpLoc = $_FILES["nCapa"]["tmp_name"];
		$data = date( 'YmdHis' );
		$realname = $data.$fileName;
		// Path and file name
		//$pathAndName = "img/".$realname;
		// Run the move_uploaded_file() function here
		//$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
		$upload = $s3->upload($bucket,
				  $realname,
				  fopen($_FILES["nCapa"]["tmp_name"], 'rb'),
				  'public-read');
		$model->setUrlAvatar($realname);
	}

	if(!$_GET['update'])
		$nId = $model->novaNoticia();
	else{
		$update = $model->updateNoticia();
	}

	if($nId){

		$media->setNoticiaId($nId);

		if($_POST['nUrl']){
			$media->setUrlMedia($_POST['nUrl']);
			$media->setTipoMedia(1);
			$media->novaMedia();
		}

		$length = count($_FILES);
		$aux = 1;
		for($i = 0; $i <= 20; $i++){

			if($_FILES["nMedia".$aux]["name"]){

				$fileName = $_FILES["nMedia".$aux]["name"];
				$fileTmpLoc = $_FILES["nMedia".$aux]["tmp_name"];
				$data = date( 'YmdHis' );
				$realname = $data.$fileName;
				// Path and file name
				//$pathAndName = "img/".$realname;
				// Run the move_uploaded_file() function here
				//$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
				$upload = $s3->upload($bucket,
				  $realname,
				  fopen($_FILES["nMedia".$aux]["tmp_name"], 'rb'),
				  'public-read');
				$media->setUrlMedia($realname);
				$media->setFonte($_POST['nFonte'.$aux]);
				$media->setTipoMedia(2);
				$media->novaMedia();
			}
			$aux++;
		}

		header( 'Location: nova-noticia.php?status=1' );
		exit();

	}else
		if($_GET['update']){

			$media->setNoticiaId($_GET['ID']);
			$media->setUrlMedia($_POST['nUrl']);
			$media->setTipoMedia(1);
			//Se tiver url do video e ainda nao existir um id para update cria uma nova media.
			if($_POST['nUrl'] && !$_POST['urlVideo']){
				$media->novaMedia();
			}
			else{
				$media->setMediaId($_POST['urlVideo']);
				$row = $media->updateMedia();
			}
			
			$length = count($_FILES);
			$aux = 1;
			$contUpdate = $_POST['totalFotas'];
			for ($i=0; $i <= $contUpdate; $i++ ){

				if($_POST['foto_'.$i]){
					$media->setMediaId($_POST['foto_'.$i]);
					$media->deleteMedia();
				}
			}
			$cont = 1;
			for($i = 0; $i <= 20; $i++){

				if($_FILES["nMedia".$cont]["name"]){

					$fileName = $_FILES["nMedia".$cont]["name"];
					$fileTmpLoc = $_FILES["nMedia".$cont]["tmp_name"];
					$data = date( 'YmdHis' );
					$realname = $data.$fileName;
					// Path and file name
					//$pathAndName = "img/".$realname;
					// Run the move_uploaded_file() function here
					//$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
					$upload = $s3->upload($bucket,
							  $realname,
							  fopen($_FILES["nMedia".$cont]["tmp_name"], 'rb'),
							  'public-read');
					$media->setUrlMedia($realname);
					$media->setFonte($_POST['nFonte'.$cont]);
					$media->setTipoMedia(2);
					$media->novaMedia();
				}
				$cont++;
			}


		header( 'Location: nova-noticia.php?ID='.$_GET['ID']  );
		exit();
	}
}

$Mnotificia = ' active open ';
$Mnovanoticia = 'active';
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.0.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Consulfarma |  Noticías</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>


<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed ">

<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php
include 'menu.php';
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					 Widget settings form goes here
				</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Save changes</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<?php
	if($_GET['status'] == 1){
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_670267542417">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<i class="fa-lg fa fa-success"></i>
			Notícia salva com sucesso!
	</div>
	<?php
	}else
		if($_GET['status'] == '0'){
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_670267542417">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<i class="fa-lg fa fa-warning"></i>
			Nao foi possível salvar a notícia!
	</div>
	<?php
	}
	?>
	<div class="alert-erros">

	</div>
	<!-- BEGIN PAGE HEADER-->
	<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
		Notícia <small>criar uma nova notícia</small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">

			<li>
				<i class="fa fa-home"></i>
				<a href="index.php">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="javascript:;">Notícias</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="nova-noticia.php">Nova notícia</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bell"></i>Cadastrar nova notícia
			</div>
		</div>
		<div class="portlet-body form">
			<div style="width: 100%; height: 100%; position: fixed; z-index: 1030; background: none repeat scroll 0px 0px rgba(0, 0, 0, 0.75); display:none;" id="bg-noticia">
				<h4 style="color: rgb(255, 255, 255); position: relative; top: 30%; font-size: 26px; left: 36%;">Enviando...</h4>
			</div>
			<!-- BEGIN FORM-->
			<form action="nova-noticia.php<?=$action?>" class="horizontal-form" method="post" id="form-noticia" enctype="multipart/form-data">
				<div class="form-body">
					<h3 class="form-section"></h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Título</label>
								<input type="text" id="nTitulo" name="nTitulo" class="form-control" value="<?=$nTitulo?>">
								<span class="help-block">
								Título da notícia </span>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group" >
								<label class="control-label">Subtítulo</label>
								<input type="text" id="nSubtitulo" name="nSubtitulo" class="form-control" value="<?=$nSubTitulo?>">
								<span class="help-block">
								Subtítulo da notícia. </span>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Periodo da notícia</label>
								<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
									<input type="text" class="form-control" name="nInicio" id="nInicio" value="<?=$nDataInicio?>">
									<span class="input-group-addon">
									até </span>
									<input type="text" class="form-control" name="nTermino" id="nTermino" value="<?=$nDataExpira?>">
								</div>
								<!-- /input-group -->
								<span class="help-block">
								Periodo de validade da notícia </span>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Categoria</label>
								<?php
								$allcategoria = $model->getAllCategoria();
								?>
								<select id="nCategoria" name="nCategoria" class="select2_category form-control" data-placeholder="Escolha uma Categoria" tabindex="1">
									<?php
									foreach($allcategoria as $Cid => $Cvalues){
											if($categoriaId == $Cid)
												$sCategoria = 'selected';
											else
												$sCategoria = '';
									?>
										<option value="<?=$Cid?>" <?=$sCategoria?>><?=$Cvalues?></option>

									<?php
									}
									?>
								</select>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<!--/span-->
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label">Tipo de Layout</label>
								<div class="radio-list" id="opt-layout">
									<?php
									$tipoCheck = '';
									if(!$_GET['ID']){
										$tipoCheck = 'checked';
									}
									?>
									<label class="radio-inline">
										<input type="radio" name="nLayout" id="nLayout1" value="1" <?=$tipoCheck?>>
										<img height="100px" width="100px;" src="img/tipo_1.png">
									</label>
									<label class="radio-inline">
										<input type="radio" name="nLayout" id="nLayout2" value="2">
										<img height="100px" width="100px;" src="img/tipo_2.png">
									</label>
									<label class="radio-inline">
										<input type="radio" name="nLayout" id="nLayout3" value="3">
										<img height="100px" width="100px;" src="img/tipo_3.png">
									</label>
									<label class="radio-inline">
										<input type="radio" name="nLayout" id="nLayout4" value="4">
										<img height="100px" width="100px;" src="img/tipo_4.png">
									</label>
								</div>
								<span class="help-block">
								Layout que será apresentado no mobile </span>
							</div>
						</div>
						<!--/span-->

					</div>
					<div class="row">
						<!--/span-->
					<div class="col-md-6">
							<div class="form-group">
								<p>Foto de capa</p>
								<div class="fileinput fileinput-new" data-provides="fileinput" >
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
										<img src="<?=$avatar?>" alt=""/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
									</div>
									<div>
										<span class="btn default btn-file">
										<span class="fileinput-new">
										Selecione image </span>
										<span class="fileinput-exists">
										Trocar </span>
										<input type="file" name="nCapa" id="nCapa" data-edit="<?=$dataedit?>">
										</span>
										<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
										Remover </a>
									</div>
									<span class="help-block"><span class="label label-danger">
															Atençao! </span> O tamanho da foto precisa ser 250 × 141.</span>
								</div>

							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group" >
								<label class="control-label">Fonte</label>
								<input type="text" id="nFonte" name="nFonte" class="form-control" value="<?=$nFonte?>">
								<span class="help-block">
								Fonte da notícia. </span>
							</div>
						</div>
					</div>
				<h3 class="form-section"></h3>
				<div class="row">
						<!--/span-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label" style="margin-left:15px">Descricao</label></br>
							<div class="col-md-10">
								<textarea class="wysihtml5 form-control" rows="15" id="nDescricao" name="nDescricao"><?=$nDescricao?></textarea>
							</div>
						</div>
					</div>
				</div>
					<!--/row-->
					<h3 class="form-section">Mídias</h3>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label>URL Video (Youtube)</label>
								<input name="nUrl" id="nVideo" type="text" class="form-control" value="<?=$nVideo?>">
								<input type="hidden" name="urlVideo" value="<?=$nVideoId?>"/>
							</div>
						</div>
					</div>

					<div class="row form-fotos">
						<span class="help-block"><span class="label label-danger">
							Atençao! </span> O tamanho da foto precisa ser <span class="label label-default">315 × 250</span> ou <span class="label label-default">200 × 300</span>.
						</span>
						<?php
						if((int)$_GET['ID'])
							$display = "display:none;";
						?>
						<div class="col-md-3 form-init" style="<?=$display?>">
							<div class="form-group">
								<p class="title-foto">Foto</p>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
										<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
									</div>
									<div>
										<span class="btn default btn-file">
										<span class="fileinput-new">
										Selecione image </span>
										<span class="fileinput-exists">
										Trocar </span>
										<input type="file" name="nMedia1" id="nMedia1">
										</span>
										<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
										Remover </a>
									</div>
									</br>
									 <input type="text" name="nFonte1"  class="form-control"/>
								</div>

							</div>
						</div>
						<?php
						if ((int)$_GET['ID']){

							$contFoto = 0;
							foreach($noticiaMedia as $idM => $nMedia){
								if($nMedia->getTipoMedia() == 2){

						?>
						<div class="col-md-3">
							<div class="form-group">
								<p class="title-foto">Foto</p>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" id="img-<?=$contFoto?>">
										<img src="https://s3-sa-east-1.amazonaws.com/consulfarmanoticia/medias/<?=$nMedia->getUrlMedia()?>" alt=""/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
									</div>
									</br>
									 <span>
									 	<input type="checkbox" value="<?=$nMedia->getMediaId()?>" name="foto_<?=$contFoto?>"/>
									 	Excluir
									 </span>
								</div>
								<input type="hidden" name="foto_ref_<?=$contFoto?>" value="<?=$nMedia->getMediaId()?>"/>
							</div>
						</div>
						<?php
								$contFoto++;
								}
							}

						}
						?>
					</div>
					<input type="hidden" value="<?=$contFoto?>" name="totalFotas"/>
					<!--/row-->
					<div class="row form-section">
						<div class="col-md-6">
							<div class="form-group">
								<button type="button" class="btn btn-info" id="add-foto"><i class="fa fa-plus"></i> Adicionar mais fotos</button>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group" >
								<label class="control-label">Notificar Usuários</label>
								<div class="radio-list" id="opt-layout">
									<label class="radio-inline">
										<input type="radio" name="nNotificar" id="nf1" value="1" checked="checked">
										Nao
									</label>
									<label class="radio-inline">
										<input type="radio" name="nNotificar" id="nf2" value="2">
										Sim
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-actions right">
					<button type="button" class="btn default">Cancelar</button>
					<button type="submit" id="btn-salvar" class="btn blue"><i class="fa fa-check"></i> Salvar</button>
				</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 Consulfarma.
	</div>
	<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="js/noticias.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
   	Noticias.init();
});
</script>
<?php
if((int)$_GET['ID']){
?>
<script>
jQuery(document).ready(function() {

	$("#uniform-nLayout1 span" ).removeClass('checked');
   $('#uniform-nLayout<?=$noticia->getTipoNoticia()?> span').attr('class','checked');
   $('#nLayout<?=$noticia->getTipoNoticia()?>').attr( 'checked', true );

});
</script>
<?php
}
?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>