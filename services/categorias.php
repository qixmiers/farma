<?php
session_start();
use model\datamapper\ServicesMapper;
use model\activerecord\ServicesModel;
use model\datamapper\Db;
$data = json_decode(file_get_contents('php://input'), true);
//ini_set('display_errors', true);

if($data['token'] === '2867050d60351edea68de25bef0b3320'){

	$_SESSION['token'] = '2867050d60351edea68de25bef0b3320';
	require_once '../autoload.php';
	$_SESSION['token'] = null;

	$config = parse_ini_file('../db.ini');
	$db = Db::getInstance($config);

	$dmNoticia = new ServicesMapper($db);
	$model = new ServicesModel();
	$model->setDm($dmNoticia);
	$noticias = Array();
	if($data['acao'] == 'deletar'){

		$resultado = $model->deleteCategoria((int)$data['reg-id'],(int)$data['categoria']);
	}
	elseif ($data['acao'] == 'cadastrar'){
		$resultado = $model->insertCategoria((int)$data['reg-id'],(int)$data['categoria']);
	}
	echo $resultado;
}else{
	echo json_encode(Array('status' => 3, 'mensagem'=> 'Token invalido' ));
}
?>