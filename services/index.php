<?php
session_start();
use model\datamapper\ServicesMapper;
use model\activerecord\ServicesModel;
use model\datamapper\Db;
$data = json_decode(file_get_contents('php://input'), true);
//ini_set('display_errors', true);
if($data['token'] === '2867050d60351edea68de25bef0b3320'){

	$_SESSION['token'] = '2867050d60351edea68de25bef0b3320';
	require_once '../autoload.php';
	$_SESSION['token'] = null;

	$config = parse_ini_file('../db.ini');
	$db = Db::getInstance($config);

	$dmNoticia = new ServicesMapper($db);
	$model = new ServicesModel();
	$model->setDm($dmNoticia);
	$noticias = Array();
	if((int)$_GET['categoria']){
		$noticias = $model->dados(Array(
			'categoria_id' => $_GET['categoria'],
			'pagina' => $_GET['pagina'],
			'quantidade' => $_GET['quantidade']
		));
	}
	elseif ($_GET['tipo'] == 'detalhes'){
		$noticias = $model->dados(Array(
			'id' => $_GET['id']
		));
	}
	echo $noticias;
}else{
	echo json_encode(Array('status' => 3, 'mensagem'=> 'Token invalido' ));
}
?>