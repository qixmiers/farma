<?php
namespace model\datamapper;

use model\datagateway\Usuario;
use model\datagateway\Usuarios;
use model\datamapper\Db;

class UsuarioMapper
{
  /**
   * @var Db
   */
  private $db = null;

  public function __construct(Db $db)
  {
    $this->db = $db;
  }

  public function find($id)
  {
  	if($id)
    	return $this->fetchAll($id)->current();
  	
  	return $this->fetchAll($id);
  	
  }

  public function fetchAll($where)
  {
    
  	$where = empty($where) ? ' WHERE situacao IN (0,1) ' : " WHERE id = $where";
    $sql = 'SELECT * FROM usuarios' . $where;
    $rs = $this->db->query($sql);
    $usuarios = new Usuarios(array());
    
  	foreach ($rs as $row)
    {
    
      $usuario = new Usuario();
      $usuario->setUserId($row['id']);
      $usuario->setNome($row['nome']);
      $usuario->setUsername($row['username']);
      $usuario->setHash($row['pass']);
      $usuario->setStatus($row['situacao']);
      $usuario->setEmail($row['email']);
      $usuario->setToken($row['token']);
      $usuario->setAvatar($row['avatar']);
      $usuarios->append($usuario);
    }
    
    return $usuarios;
  }

  public function insert(Usuario $usuario)
  {
    $sql = "INSERT INTO usuarios(nome,username,pass,email,data_inserido,data_alteracao,situacao) 
    VALUES ('{$usuario->getNome()}','{$usuario->getUsername()}', '{$usuario->getPass()}', '{$usuario->getEmail()}', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."', 0)";
    return $this->db->exec($sql);
  }

  public function update(Usuario $usuario)
  {
  	if($usuario->getPass())
  		$senha = ", pass = '{$usuario->getPass()}' ";
  	
    $sql = "UPDATE usuarios SET nome = '{$usuario->getNome()}', username = '{$usuario->getUsername()}',
   	email = '{$usuario->getEmail()}', data_alteracao = '".date('Y-m-d H:i:s')."' {$senha}  WHERE id = {$usuario->getUserId()}";
    return $this->db->exec($sql);
  }
  
  public function updateSenha(Usuario $usuario){
  	
  	$sql = "UPDATE usuarios SET pass = '{$usuario->getPass()}'  WHERE id = {$usuario->getUserId()}";
  	return $this->db->exec($sql);
  	 
  }
  
  public function updateFoto(Usuario $usuario){
  	 
  	$sql = "UPDATE usuarios SET avatar = '{$usuario->getAvatar()}'  WHERE id = {$usuario->getUserId()}";
  	return $this->db->exec($sql);
  
  }

  public function delete(Usuario $usuario)
  {
    $sql = "UPDATE usuarios SET situacao = 2 WHERE id = {$usuario->getUserId()}";
    return $this->db->exec($sql);
  }
}