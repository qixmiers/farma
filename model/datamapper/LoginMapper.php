<?php
namespace model\datamapper;

use model\datagateway\Usuario;
use model\datagateway\Usuarios;
use model\datamapper\Db;

class LoginMapper
{
  /**
   * @var Db
   */
  private $db = null;

  public function __construct(Db $db)
  {
    $this->db = $db;
  }

  public function find(Array $get)
  {

  	$where = empty($get['token']) ? '' : " token = '".$get['token']."' ";
  	
  	if(!$where){
  		
  		$pos = strpos($get['username'], '@');
  		
  		if($pos === false)
  		{
  			
  			$where = " username = '".$get['username']."' ";
  		}
  		else
  		{
  			$where = " email = '".$get['username']."' ";
  		}
  		
  		if(!$where)
  			return false;
  		
  	}
  	
    return $this->fetchAll($where)->current();
  }

  public function fetchAll($where)
  {
    $where = empty($where) ? '' : " WHERE $where";
    $sql = 'SELECT * FROM usuarios' . $where;
    $rs = $this->db->query($sql);
    $usuarios = new Usuarios(array());
 
    foreach ($rs as $row)
    {
    
      $usuario = new Usuario();
      $usuario->setUserId($row['id']);
      $usuario->setNome($row['nome']);
      $usuario->setUsername($row['username']);
      $usuario->setHash($row['pass']);
      $usuario->setStatus($row['situacao']);
      $usuario->setEmail($row['email']);
      $usuario->setToken($row['token']);
      $usuario->setAvatar($row['avatar']);
      $usuario->setCategoriaId($row['id_categoria_usuario']);
      
      $usuarios->append($usuario);
    }
    
  
    return $usuarios;
  }

  public function update(Usuario $usuario)
  {
    $sql = "UPDATE usuarios SET token = '{$usuario->getToken()}' WHERE email = '{$usuario->getEmail()}' AND id = {$usuario->getUserId()}";
    return $this->db->exec($sql);
  }

  
}