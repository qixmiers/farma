<?php
namespace model\datamapper;

class Db extends \PDO
{
  public static function getInstance(array $config)
  {
    $dsn = $config['dsn'];
    $username = $config['username'];
    $passwd = $config['passwd'];
    return new Db($dsn, $username, $passwd,array(MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
  }
}