<?php
namespace model\datamapper;

use model\datamapper\Db;

class Token{
	
	/**
	 * @var Db
	 */
	private $db = null;
	
	public function __construct(Db $db)
	{
		$this->db = $db;
	}
	
	public function getToken($userid){
		
		$sql = "Select token FROM usuarios WHERE id = {$userid} ";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		return $stmt->fetch();
	}

}
?>