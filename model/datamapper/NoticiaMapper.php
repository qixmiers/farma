<?php
namespace model\datamapper;

use model\datagateway\Noticia;
use model\datagateway\Noticias;
use model\datamapper\Db;

class NoticiaMapper{

	/**
	 * @var Db
	 */

	private $db = null;

	public function __construct(Db $db){
		$this->db = $db;
	}

	public function find($id){

		if($id)
			return $this->fetchAll($id)->current();

		return $this->fetchAll($id);

	}

	public function fetchAll($where)
  	{

	  	$where = empty($where) ? ' order by id DESC' : " WHERE id = $where order by id DESC";
	    $sql = 'SELECT * FROM noticia' . $where;
	    $rs = $this->db->query($sql);
	    $noticias = new Noticias(array());
		$categorias = $this->getCategoria();
	  	foreach ($rs as $row)
	    {

	      $noticia = new Noticia();
	      $noticia->setNoticiaId($row['id']);
	      $noticia->setTitulo($row['titulo']);
	      $noticia->setSubTitulo($row['sub_titulo']);
	      $noticia->setDataInserido($row['data_inserido']);
	      $noticia->setDataExpira($row['data_expira']);
	      $noticia->setUrlAvatar($row['url_avatar']);
	      $noticia->setTipoNoticia($row['tipo_noticia']);
	      $noticia->setDescricao($row['descricao']);
	      $noticia->setFonte($row['fonte']);
	      $noticia->setCategoriaId($row['categoria_id']);
	      $noticia->setUserId($row['user_id']);
	      $noticias->append($noticia);
	    }

	    return $noticias;
  	}

  	public function fetchAllFilter(array $where)
  	{
  		if($where['data_inicio'])
  			$wDataInicio = ' AND data_inserido >= "'.$where['data_inicio'].'" ';
  		if($where['data_termino'])
  			$wDataTermino = ' AND data_expira <= "'.$where['data_termino'].'" ';
  		if($where['sTitulo'])
  			$wTitulo = ' AND titulo like("%'.$where['sTitulo'].'%")';
  		if($where['sSubtitulo'])
  			$wSubtitulo = ' AND sub_titulo like("%'.$where['sSubtitulo'].'%")';
  		if($where['nCategoria'] > 0)
  			$wCategoria = ' AND categoria_id = '.$where['nCategoria'];
  		if($where['noticia_status']){
  			if($where['noticia_status'] == 'ativos')
  				$wStatus = '  AND data_expira > NOW() OR data_expira = "0000-00-00 00:00:00"';
  			elseif ($where['noticia_status'] == 'desativos')
  				$wStatus = ' AND data_expira < NOW() AND data_expira <> "0000-00-00 00:00:00" ';
  		}

  		$filtro = $wNoticia.$wDataInicio.$wDataTermino.$wTitulo.$wSubtitulo.$wCategoria.$wStatus;

  		$sql = 'SELECT * FROM noticia WHERE user_id > 0 ' . $filtro.' order by id DESC';
  		$rs = $this->db->query($sql);
  		$noticias = new Noticias(array());
  		$categorias = $this->getCategoria();
  		foreach ($rs as $row)
  		{

  			$noticia = new Noticia();
  			$noticia->setNoticiaId($row['id']);
  			$noticia->setTitulo($row['titulo']);
  			$noticia->setSubTitulo($row['sub_titulo']);
  			$noticia->setDataInserido($row['data_inserido']);
  			$noticia->setDataExpira($row['data_expira']);
  			$noticia->setUrlAvatar($row['url_avatar']);
  			$noticia->setTipoNoticia($row['tipo_noticia']);
  			$noticia->setDescricao($row['descricao']);
  			$noticia->setFonte($row['fonte']);
  			$noticia->setCategoriaId($row['categoria_id']);
  			$noticia->setUserId($row['user_id']);
  			$noticias->append($noticia);
  		}

  		return $noticias;
  	}

  	public function getCategoria(){

  		$sql = 'SELECT * FROM categoria_noticia';
  		$rs = $this->db->query($sql);
  		foreach ($rs as $row)
  		{
  			$categoria[$row['id']]=utf8_encode($row['nome']);
  		}

  		return $categoria;
  	}

  	public function create(Noticia $noticia){

  		$sql = "INSERT INTO noticia (titulo, sub_titulo, data_inserido, data_expira, url_avatar, tipo_noticia, descricao, fonte, categoria_id, user_id)
  		VALUES(:Titulo, :SubTitulo, :DataInserido, :DataExpira,:UrlAvatar,:TipoNoticia, :Descricao, :Fonte, :CategoriaId, :UserId)";
  		/*
  		$sql = "INSERT INTO noticia (titulo, sub_titulo, data_inserido, data_expira, url_avatar, tipo_noticia, descricao, fonte, categoria_id, user_id)
  				VALUES('{$noticia->getTitulo()}', '{$noticia->getSubTitulo()}', '{$noticia->getDataInserido()}', '{$noticia->getDataExpira()}', '{$noticia->getUrlAvatar()}',
	    		'{$noticia->getTipoNoticia()}', '{$noticia->getDescricao()}', '{$noticia->getFonte()}', {$noticia->getCategoriaId()}, {$noticia->getUserId()})";
		*/
  		$dados = Array(
  			":Titulo" => $noticia->getTitulo(),
  			":SubTitulo" => $noticia->getSubTitulo(),
  			":DataInserido" => $noticia->getDataInserido(),
  			":DataExpira" => $noticia->getDataExpira(),
  			":UrlAvatar" => $noticia->getUrlAvatar(),
	    	":TipoNoticia" => $noticia->getTipoNoticia(),
  			":Descricao" => $noticia->getDescricao(),
  			":Fonte" => $noticia->getFonte(),
  			":CategoriaId" => $noticia->getCategoriaId(),
  			":UserId" => $noticia->getUserId());
  		$stmt = $this->db->prepare($sql);
  		$stmt->execute($dados);

  		return $this->db->lastInsertId();
  	}

  	public function update(Noticia $noticia){

  		if($noticia->getUrlAvatar()){
  			$urlavatar = " ,url_avatar = :UrlAvatar";
  		}

  		$sql = "UPDATE noticia SET titulo = :Titulo, sub_titulo = :SubTitulo, data_inserido = :DataInserido,
  				data_expira = :DataExpira, tipo_noticia = :TipoNoticia, descricao = :Descricao, fonte = :Fonte, categoria_id = :CategoriaId, user_id = :UserId
  				$urlavatar
  				WHERE id = {$noticia->getNoticiaId()} ";


		$dados = Array(
  			":Titulo" => $noticia->getTitulo(),
  			":SubTitulo" => $noticia->getSubTitulo(),
  			":DataInserido" => $noticia->getDataInserido(),
  			":DataExpira" => $noticia->getDataExpira(),
  			":TipoNoticia" => $noticia->getTipoNoticia(),
  			":Descricao" => $noticia->getDescricao(),
  			":Fonte" => $noticia->getFonte(),
  			":CategoriaId" => $noticia->getCategoriaId(),
  			":UserId" => $noticia->getUserId()
		);

  		if($urlavatar){
			$dados = Array(
	  			":Titulo" => $noticia->getTitulo(),
	  			":SubTitulo" => $noticia->getSubTitulo(),
	  			":DataInserido" => $noticia->getDataInserido(),
	  			":DataExpira" => $noticia->getDataExpira(),
	  			":TipoNoticia" => $noticia->getTipoNoticia(),
	  			":Descricao" => $noticia->getDescricao(),
	  			":Fonte" => $noticia->getFonte(),
	  			":CategoriaId" => $noticia->getCategoriaId(),
	  			":UserId" => $noticia->getUserId(),
				":UrlAvatar" => $noticia->getUrlAvatar()
			);
		}

  		$stmt = $this->db->prepare($sql);
  		return $stmt->execute($dados);

  	}

  	public function deletarNoticia(Noticia $noticia){

  		$sql = "DELETE FROM noticia WHERE id = {$noticia->getNoticiaId()}";
  		return $this->db->exec($sql);
  	}

  	public function MinhasNoticia(Noticia $noticia){

  		$sql = "SELECT
				(SELECT COUNT(id) from noticia WHERE data_expira < NOW() AND data_expira <> '0000-00-00 00:00:00') as desativas,
				(SELECT COUNT(id) from noticia WHERE data_expira > NOW() OR data_expira = '0000-00-00 00:00:00' ) as ativas
				FROM noticia WHERE user_id = {$noticia->getUserId()}";
  		$stmt = $this->db->prepare($sql);
  		$stmt->execute();
  		return $stmt->fetch();
  	}




}
?>