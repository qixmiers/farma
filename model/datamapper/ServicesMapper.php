<?php
namespace model\datamapper;

use model\datagateway\Noticia;
use model\datagateway\Noticias;
use model\datagateway\Media;
use model\datagateway\Medias;
use model\datamapper\Db;

class ServicesMapper{
	/**
	 * @var Db
	 */
	private $db = null;

	public function __construct(Db $db){
		$this->db = $db;
	}

	public function getNoticias(array $where){
		$noticias = Array();
		$categoria = empty($where['categoria_id']) ? '' : " AND categoria_id = {$where['categoria_id']}";

		$ponteiro = ($where['quantidade']*$where['pagina']) - $where['quantidade'];

		$quantidade = empty($where['quantidade']) ? ' ORDER BY id DESC ' : " ORDER BY id DESC Limit {$ponteiro},{$where['quantidade']}";

		$sql = "SELECT *,(select count(id) from noticia where data_expira >= NOW() OR data_expira = '0000-00-00 00:00:00') as totalnoticia FROM noticia WHERE data_expira >= NOW() OR data_expira = '0000-00-00 00:00:00' ".$categoria.$quantidade;

		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		while($fields = $stmt->fetch()){
			$noticias[] = Array(
				'id' => $fields['id'],
				'titulo' => $fields['titulo'],
				'sub_titulo' => $fields['sub_titulo'],
				'data_inserido' => $this->getdataBR($fields['data_inserido']),
				'url_avatar' => 'https://s3-sa-east-1.amazonaws.com/consulfarmanoticia/medias/'.$fields['url_avatar'],
				'tipo_noticia' => $fields['tipo_noticia'],
				'descricao' => $fields['descricao'],
				'fonte' => $fields['fonte'],
				'categoria_id' => $fields['categoria_id'],
				'medias' => $this->getMidia($fields['id']),
			);

		}
		if(!$noticias AND $where['pagina'] > 1){
			$noticias[0] = Array(
				'totalnoticia' => 1
			);
		}

		return $noticias;
	}
	public function getMidia($id){

		$sql = "SELECT * FROM media WHERE noticia_id = {$id}";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$medias = array();
		while($fields = $stmt->fetch()){

			if($fields['tipo_media'] == 1){
				$url = explode('watch?v=',$fields['url_midia']);
				$urlavatar = $url[1];
			}
			elseif ($fields['tipo_media'] == 2)
				$urlavatar = 'https://s3-sa-east-1.amazonaws.com/consulfarmanoticia/medias/'.$fields['url_midia'];

			$medias[] = Array(
				'tipo_midia' => $fields['tipo_media'],
				'url_midia' => $urlavatar,
				'fonte' => $fields['fonte']
			);
		}
		return $medias;
	}

	public function getDetalhes($id){

		$noticias = Array();
		$sql = "SELECT * FROM noticia WHERE data_expira >= NOW() OR data_expira = '0000-00-00 00:00:00' AND id = {$id['id']} ";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		while($fields = $stmt->fetch()){
			$noticias[] = Array(
					'id' => $fields['id'],
					'titulo' => $fields['titulo'],
					'sub_titulo' => $fields['sub_titulo'],
					'data_inserido' => $this->getdataBR($fields['data_inserido']),
					'url_avatar' => 'https://s3-sa-east-1.amazonaws.com/consulfarmanoticia/medias/'.$fields['url_avatar'],
					'tipo_noticia' => $fields['tipo_noticia'],
					'descricao' => $fields['descricao'],
					'categoria_id' => $fields['categoria_id'],
					'medias' => $this->getMidia($fields['id'])
			);

		}

		return $noticias;
	}

	public function getdataBR($data){

		$ex = explode('-',$data);

		$hora = explode(' ',$ex[2] );

		return  $hora[0].'/'.$ex[1].'/'.$ex[0];


	}

	public function deleteCategoria($id,$categoria){

		$sql = "DELETE FROM register_categorias WHERE gcm_register = {$id} AND categoria = {$categoria}";
		$stmt = $this->db->prepare($sql);
		return $stmt->execute();

	}

	public function insertCategoria($id,$categoria){

		$sql = "INSERT INTO register_categorias(gcm_register,categoria)VALUES({$id},{$categoria})";
		$stmt = $this->db->prepare($sql);
		return $stmt->execute();
	}
}
?>