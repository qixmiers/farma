<?php
namespace model\datamapper;

use model\datagateway\Media;
use model\datagateway\Medias;
use model\datamapper\Db;

class MediaMapper{
	
	/**
	 * @var db
	 */
	
	private $db = null;
	
	public function __construct(Db $db){
		
		$this->db = $db;
	}
	
	public function find($id)
	  {
	  	if(!$id)
	    	return $this->fetchAll($id)->current();
	  	
	  	return $this->fetchAll($id);
	  	
	  }
	
	  public function fetchAll($where)
	  {
	    
	  	$where = empty($where) ? '' : " WHERE noticia_id = $where order by tipo_media ASC";
	    $sql = 'SELECT * FROM media' . $where;
	    $rs = $this->db->query($sql);
	    $medias = new Medias(array());
	    
	  	foreach ($rs as $row)
	    {
	    
	      $media = new Media();
	      $media->setMediaId($row['id']);
	      $media->setTipoMedia($row['tipo_media']);
	      $media->setUrlMedia($row['url_midia']);
	      $media->setFonte($row['fonte']);
	      $media->setNoticiaId($row['noticiaid']);
	      $medias->append($media);
	    }
	    
	    return $medias;
	  }
	  
	  public function create(Media $media){
	  	
	  	$sql = "INSERT INTO media (tipo_media, url_midia, fonte, noticia_id)
	  			VALUES({$media->getTipoMedia()},'{$media->getUrlMedia()}', '{$media->getFonte()}',{$media->getNoticiaId()})";
	  	return $this->db->exec($sql);
	  	
	  }
	  
	  public function update(Media $media){
	  	
	  	$sql = "UPDATE media SET tipo_media = {$media->getTipoMedia()}, url_midia = '{$media->getUrlMedia()}', fonte = '{$media->getFonte()}'
	  			WHERE id = {$media->getMediaId()}";
	  	return $this->db->exec($sql);
	  }
	  
	  public function deleteMedia(Media $media){
		
	  	$sql = "DELETE FROM media WHERE id = {$media->getMediaId()} ";
	  	return $this->db->exec($sql);
	  
	  }
	  
	  public function deleteMediaNoticia(Media $media){
	  	
	  	$sql = "DELETE FROM media WHERE noticia_id = {$media->getNoticiaId()} ";
	  	return $this->db->exec($sql);
	  }
	  
}