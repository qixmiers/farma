<?php
namespace model\activerecord;

use model\datagateway\Usuario;
use model\datamapper\UsuarioMapper;
use model\datamapper\Bcrypt;

class UsuarioModel extends Usuario{
	
  private $dmUsuario = null;

  public function setDm(UsuarioMapper $dmUsuario)
  {
    $this->dmUsuario = $dmUsuario;
  }

  public function dados()
  {
  	
    $user = $this->dmUsuario->find($this->getUserId());
    
    if ($user == null)
    {
      return false;
    }
    
    
    return $user;
    
  }
  
  public function novoUser(){
  	
  	$this->dmUsuario->insert($this);
  }
  
  public function update()
  {
  	
  	$status = $this->dmUsuario->update($this);
  	
  	return $status;
  }
  
  public function updateFoto(){
  	
  	$status = $this->dmUsuario->updateFoto($this);
  	return $status;
  }
  
  public function updateSenha(){
  	
  	$status = $this->dmUsuario->updateSenha($this);
  	return $status;
  	
  }
  
  public function delete(){
  	
  	$status = $this->dmUsuario->delete($this);
  	return $status;
  }

}