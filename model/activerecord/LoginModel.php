<?php
namespace model\activerecord;

use model\datagateway\Usuario;
use model\datamapper\LoginMapper;
use model\datamapper\Bcrypt;

class LoginModel extends Usuario{
  private $dmLogin = null;

  public function setDm(LoginMapper $dmLogin)
  {
    $this->dmLogin = $dmLogin;
  }

  public function login()
  {
  	
  	$get = Array(
  		'token' => $this->getToken(),
  		'username' => $this->getUsername()
  	);
  	
    $user = $this->dmLogin->find($get);
    
    if ($user == null)
    {
      return false;
    }
    else
    	if($user->getUsername())
    {
    	if (Bcrypt::check($this->getPass(), $user->getHash())) {
    		$email = $user->getEmail();
    		$userid = $user->getUserId();
    		$newtoken = $this->createToken($email);
    		$this->setToken($newtoken);
    		$this->createsession($user);
    		$this->setEmail($email);
    		$this->setUserId($userid);
    	} else {
    		return false;
    	}
    	
      $this->dmLogin->update($this);
    }
    
    return true;
    
  }

  private function createsession($user)
  {
  	
  	$_SESSION['nome'] = $user->getNome();
  	$_SESSION['email'] = $user->getEmail();
  	$_SESSION['username'] = $user->getUsername();
  	$_SESSION['token'] = $this->getToken();
  	$_SESSION['id'] = $user->getUserId();
  	$_SESSION['avatar'] = $user->getAvatar();
  	$_SESSION['categoria'] = $user->getCategoriaId();
  }
  
  private function createToken($email){
  	
  	$datahora = date('YmdHis');
  	$token = md5($datahora.$email);
  	
  	return $token;
  }

  public function recuperarSenha(){
  	$get = Array(
  			'token' => $this->getToken(),
  			'username' => $this->getUsername()
  	);
  	 
  	$user = $this->dmLogin->find($get);
  
  	if ($user == null)
  	{
  		return false;
  	}
  	else
  	if($user->getUsername())
  	{
  		
  		$token = $this->createToken($user->getEmail());
  		$this->setToken($token);
  		$email = $user->getEmail();
    	$userid = $user->getUserId();
    	$this->setEmail($email);
    	$this->setUserId($userid);
    	$this->dmLogin->update($this);
  		return Array(
  			'email' => $user->getEmail(),
  			'token' => $token
  		);
  	}
  	
  }
 
}