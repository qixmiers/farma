<?php
namespace model\activerecord;

use model\datamapper\Token;

class TokenModel{
	
	 private $id;
	 
	 public function getToken(Token $token){
	 
	 	$token = $token->getToken($this->getId());
	 	return $token;
	 
	 }
	 
	 public function setId($id){
	 	$this->id = $id;
	 }
	 private function getId(){
	 	return $this->id;
	 }
	
	
}
?>