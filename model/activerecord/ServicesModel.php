<?php
namespace model\activerecord;

use model\datamapper\ServicesMapper;

class ServicesModel{

	private $dmService = null;

	public function setDm(ServicesMapper $dmService){
		$this->dmService = $dmService;
	}

	public function dados(array $where){

		if($where['categoria_id'])
			$noticias = $this->dmService->getNoticias($where);
		elseif ($where['id'])
			$noticias = $this->dmService->getDetalhes($where);
		if($noticias == null){
			$json = json_encode(
				Array(
					'status' => 0,
					'mensagem' => 'Nenhuma noticia encontrada'
			));
		}
		elseif ($noticias[0]['id'] == null AND $noticias[0]['totalnoticia'] > 0){
			$json = json_encode(
					Array(
							'status' => 4,
							'mensagem' => 'sucesso - sem resgistros'
					));
		}
		else
			$json = json_encode(
					Array(
						'status' => 1,
						'mensagem' => 'sucesso',
						'noticias' => $noticias
			));

		return $json;
	}

	public function deleteCategoria($id,$categoria){

		$row = $this->dmService->deleteCategoria($id,$categoria);
		return json_encode(Array('status' => $row));
	}

	public function insertCategoria($id,$categoria){

		$row = $this->dmService->insertCategoria($id,$categoria);
		return json_encode(Array('status' => $row));

	}
}
?>