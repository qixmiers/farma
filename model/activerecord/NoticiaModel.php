<?php
namespace model\activerecord;

use model\datagateway\Noticia;
use model\datamapper\NoticiaMapper;

class NoticiaModel extends Noticia{

	private $dmNoticia = null;
	public $nf = false;

	public function setDm(NoticiaMapper $dmNoticia){
		$this->dmNoticia = $dmNoticia;
	}

	public function dados(){

		$noticia = $this->dmNoticia->find($this->getNoticiaId());

		if($noticia == null)
			return false;

		return $noticia;

	}

	public function dadosFilter(array $where){

		$noticia = $this->dmNoticia->fetchAllFilter($where);

		if($noticia == null)
			return false;

		return $noticia;

	}

	public function novaNoticia(){

		$id = $this->dmNoticia->create($this);

		if($this->nf === true){
			$this->notificarUsuario($id);
		}

		return $id;
	}

	public function updateNoticia(){

		$row = $this->dmNoticia->update($this);
		if($this->nf === true){
			$this->notificarUsuario($this->getNoticiaId());
		}
		return $row;
	}

	public function getdata($data){

		$ex = explode('/',$data);

		return $ex[2].'-'.$ex[1].'-'.$ex[0];
	}
	public function getdataBR($data){

		$ex = explode('-',$data);

		$hora = explode(' ',$ex[2] );

		return  $hora[0].'/'.$ex[1].'/'.$ex[0];


	}

	public function getAllCategoria(){

		$noticia = $this->dmNoticia->getCategoria($this);
		return $noticia;
	}

	public function deleteNoticia(){
		$noticia = $this->dmNoticia->deletarNoticia($this);
		return $noticia;
	}

	public function sumarioNoticia(){
		$noticia = $this->dmNoticia->MinhasNoticia($this);
		return $noticia;
	}

	private function notificarUsuario($noticiaID){

		$data = array (
				'method' => 'send-gcm-message',
				'title' => substr($this->getTitulo(),0,30),
				'author' => substr($this->getSubTitulo(),0,30),
				'message' => intval($noticiaID),
				'token' => '2867050d60351edea68de25bef0b3320',
				'categoriaID' => $this->getCategoriaId()
		);

		$data_string = json_encode($data);

		$ch = curl_init('http://www.cubomob.com.br/consulfarmanoticias/model/gcm-php/ctrl/CtrlGcm.php');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);

	}

}
?>