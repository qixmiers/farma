<?php 
namespace model\activerecord;

use model\datagateway\Media;
use model\datamapper\MediaMapper;

class MediaModel extends Media{
	
	private $dmMedia = null;
	
	public function setDm(MediaMapper $dmMedia){
		$this->dmMedia = $dmMedia;
	}
	
	public function dados(){
		
		$media = $this->dmMedia->find($this->getMediaId());
		
		if($media == null)
			return false;
		
		return $media;
		
	}
	
	public function novaMedia(){
		
		$id = $this->dmMedia->create($this);
		
		return $id;
	}
	
	public function updateMedia(){
		
		$row = $this->dmMedia->update($this);
		return $row;
	}
	
	public function deleteMedia(){
		
		$row = $this->dmMedia->update($this);
		return $row;
	}
	public function deleteMediaNoticia(){
	
		$row = $this->dmMedia->deleteMediaNoticia($this);
		return $row;
	}
	
}
?>