<?php
	date_default_timezone_set("America/Sao_Paulo");
	require_once('Database.php');

	class DaoGcm {
		private $conn;	

		public function __construct(){
			$this->conn = new Database();
		}
		public function __destruct(){
			$this->conn->getConn()->close();
		}


		public function save($gcm){
			$data = array();
			$data[] = $gcm->getRegistrationId();
			$data[] = $gcm->getEmail();
			$data[] = date('Y-m-d H:i:s');
			$query = <<<SQL
				INSERT INTO
					gcm_register(registration_id, email, data_inserido, ativo)
					VALUES("$data[0]","$data[1]","$data[2]", "1")
SQL;
			$this->conn->getConn()->query($query);
			$id = $this->conn->getConn()->insert_id;
			$this->saveCategorias($id);
			return(Array('feedback' => 1, 'registerID' => $id, 'mensagem' => 'Registrado com sucesso'));
		}

		public function saveCategorias($id){
			$categorias = Array();
			$categorias = $this->allCategorias();
			foreach($categorias as $key => $categoria){
				$query = <<<SQL
					INSERT INTO
						register_categorias(gcm_register,categoria)
							VALUES({$id},{$categoria});
SQL;
				$this->conn->getConn()->query($query);
			}

		}

		private function allCategorias(){

			$query = <<<SQL
				SELECT
					*
					FROM
						categoria_noticia

SQL;
			$result = $this->conn->getConn()->query($query);
			while($row = $result->fetch_assoc()) {
				$categorias[] = $row['id'];
			}
			return $categorias;

		}


		public function update($gcm){
			$data = array();
			$data[] = $gcm->getRegistrationId();
			$data[] = $gcm->getNewRegistrationId();
			$query = <<<SQL
				UPDATE
					gcm_register
					SET
						registration_id = "$data[1]"
					WHERE
						registration_id LIKE "$data[0]"
SQL;
			$this->conn->getConn()->query($query);
			return($this->conn->getConn()->affected_rows);
		}


		public function delete($gcm){
			$data = array();
			$data[] = $gcm->getRegistrationId();
			$data[] = date('Y-m-d H:i:s');
			$query = <<<SQL
				UPDATE
					gcm_register
					SET
						ativo = 0,
						data_alteracao = "$data[1]"
					WHERE
						registration_id LIKE "$data[0]"
SQL;
			$this->conn->getConn()->query($query);
			return($this->conn->getConn()->affected_rows);
		}
		
		public function ativar($gcm){
			$data = array();
			$data[] = $gcm->getRegistrationId();
			$data[] = date('Y-m-d H:i:s');
			$query = <<<SQL
				UPDATE
					gcm_register
					SET
						ativo = 1,
						data_alteracao = "$data[1]"
					WHERE
						registration_id LIKE "$data[0]" AND ativo = 0
SQL;
			$this->conn->getConn()->query($query);
			return($this->conn->getConn()->affected_rows);
		}


		public function verify($gcm){
			$data = array();
			$data[] = $gcm->getRegistrationId();
			$query = <<<SQL
				SELECT
					registration_id, id
					FROM
						gcm_register
					WHERE
						registration_id LIKE "$data[0]"
					LIMIT 1
SQL;
			$result = $this->conn->getConn()->query($query);
			$data = $result->fetch_array(MYSQLI_ASSOC);
			$result->free();
			if($data['registration_id']){
				$data['feedback'] = 0;
				$affected_row = $this->ativar($gcm);
				if($affected_row > 0)
					$data['feedback'] = 3;
			}
			else
				$data['feedback'] = 1;
				
			return($data);
		}

		//CategoriaID é o tipo de noticia.
		public function getAll($categoriaId){
			$query = <<<SQL
				SELECT
					registration_id
					FROM
						gcm_register
					WHERE 
						ativo = 1 
					AND 
						id 
					IN 
					(SELECT gcm_register FROM register_categorias WHERE categoria = "$categoriaId")	
SQL;
			$result = $this->conn->getConn()->query($query);
			$arrayGcm = array();
			while($data = $result->fetch_object()){
				$arrayGcm[] = new Gcm($data->registration_id);
			}
			$result->free();
			return($arrayGcm);
		}
	}
?>