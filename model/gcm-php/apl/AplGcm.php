<?php
	require_once('../cdp/Gcm.php');
	require_once('../cgd/DaoGcm.php');
	
	class AplGcm {
		private $dao;
		
		public function __construct(){
			$this->dao = new DaoGcm();
		}
		public function __destruct(){
			// OBJ
		}
		
		public function save($gcm){
			$return = 0;
			$response = $this->dao->verify($gcm);
			if($response['feedback'] === 1){
				return ($this->dao->save($gcm));
			}
			elseif ($response['feedback'] === 0){
				return(Array('feedback'=>0, 'registerID' => $response['id'], 'mensagem' => 'Ja registrado'));
			}
			elseif ($response['feedback'] === 3){
				return(Array('feedback'=>3, 'registerID' => $response['id'], 'mensagem' => 'Alterado com sucesso'));
			}
			
			return(Array('feedback'=>999, 'registerID' => null, 'mensagem' => 'Erro'));
		}
		
		public function update($gcm){
			$response = $this->dao->verify($gcm);
			if($response['feedback'] === 0){
				$return = $this->dao->update($gcm);
			}
			else{
				$this->delete($gcm);
			}
			return($return);
		}
		
		public function delete($gcm){
			$return = $this->dao->delete($gcm);
			return($return);
		}
		
		public function getAll($categoriaID){
			$arrayGcm = $this->dao->getAll($categoriaID);
			return($arrayGcm);
		}
	}
?>