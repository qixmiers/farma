<?php
	class Gcm {
		private $registrationId;
		private $newRegistrationId;
		private $email;
		
		public function __construct($registrationId='', $newRegistrationId='', $email){
			$this->registrationId = $registrationId;
			$this->newRegistrationId = $newRegistrationId;
			$this->email = $email;
		}
		public function __destruct(){
			// OBJ
		}
		
		public function getRegistrationId(){
			return($this->registrationId);
		}
		public function setRegistrationId($registrationId){
			$this->registrationId = $registrationId;
		}
		
		public function getNewRegistrationId(){
			return($this->newRegistrationId);
		}
		public function setNewRegistrationId($newRegistrationId){
			$this->newRegistrationId = $newRegistrationId;
		}
		
		public function getEmail(){
			return($this->email);
		}
		public function setEmail($email){
			$this->email = $email;
		}
	}
?>