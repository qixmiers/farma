<?php
namespace model\datagateway;

class Usuario
{
  private $userid;	
  private $nome;
  private $username;
  private $email;
  private $pass;
  private $hash; //Senha Gravada no Banco
  private $status;
  private $token;
  private $avatar;
  private $categoria_id;

  public function setUserId($id)
  {
  	$this->userid = $id;
  }
  
  public function getUserId()
  {
  	return $this->userid;
  }
  
  public function setNome($nome)
  {
    $this->nome = $nome;
  }

  public function getNome()
  {
    return $this->nome;
  }

  public function setUsername($username)
  {
    $this->username = $username;
  }

  public function getUsername()
  {
    return $this->username;
  }
  
  public function setEmail($email)
  {
  	$this->email = $email;
  }
  
  public function getEmail()
  {
  	return $this->email;
  }
  
  public function setPass($pass)
  {
  	$this->pass = $pass;
  }
  
  public function getPass()
  {
  	return $this->pass;
  }
  
  
  public function setHash($hash)
  {
  	$this->hash = $hash;
  }
  
  public function getHash()
  {
  	return $this->hash;
  }
  
  public function setStatus($status)
  {
  	$this->status = $status;
  }
  
  public function getStatus()
  {
  	return $this->status;
  }
  
  public function setToken($token)
  {
  	$this->token = $token;
  }
  
  public function getToken()
  {
  	return $this->token;
  }
  
  public function setAvatar($avatar)
  {
  	$this->avatar = $avatar;
  }
  
  public function getAvatar()
  {
  	return $this->avatar;
  }
  
  public function setCategoriaId($id)
  {
  	$this->categoria_id = $id;
  }
  
  public function getCategoriaId()
  {
  	return $this->categoria_id;
  }
}