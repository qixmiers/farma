<?php 
namespace model\datagateway;

class Noticia{
	
	private $noticiasid;
	private $titulo;
	private $sub_titulo;
	private $data_inserido;
	private $data_expira;
	private $url_avatar;
	private $tipo_noticia;
	private $descricao;
	private $fonte;
	private $categoria_id;
	private $user_id;
	
	public function setNoticiaId($id){
		$this->noticiasid = $id;
	}
	public function getNoticiaId(){
		return $this->noticiasid;
	}
	
	public function setTitulo($titulo){
		$this->titulo = $titulo;
	}
	public function getTitulo(){
		return $this->titulo;
	}
	
	public function setSubTitulo($subtitulo){
		$this->sub_titulo = $subtitulo;
	}
	public function getSubTitulo(){
		return $this->sub_titulo;
	}
	
	public function setDataInserido($data){
		$this->data_inserido = $data;
	}
	public function getDataInserido(){
		return $this->data_inserido;
	}
	
	public function setDataExpira($data){
		$this->data_expira = $data;
	}
	public function getDataExpira(){
		return $this->data_expira;
	}
	
	public function setUrlAvatar($url){
		$this->url_avatar = $url;
	}
	public function getUrlAvatar(){
		return $this->url_avatar;
	}
	
	public function setTipoNoticia($tiponoticia){
		$this->tipo_noticia = $tiponoticia;
	}
	public function getTipoNoticia(){
		return $this->tipo_noticia;
	}
	
	public function setDescricao($desc){
		$this->descricao = $desc;
	}
	public function getDescricao(){
		return $this->descricao;
	}
	
	public function setFonte($fonte){
		$this->fonte = $fonte;
	}
	public function getFonte(){
		return $this->fonte;
	}
	
	public function setCategoriaId($id){
		$this->categoria_id = $id;
	}
	public function getCategoriaId(){
		return $this->categoria_id;
	}
	
	public function setUserId($id){
		$this->user_id = $id;
	}
	public function getUserId(){
		return $this->user_id;
	}
}
?>