<?php 
namespace model\datagateway;

class Media{ 
	
	private $mediaid;
	private $tipo_media;
	private $url_media;
	private $fonte;
	private $noticiaid;
	
	
	public function setMediaId($id){
		$this->mediaid = $id;
	}
	public function getMediaId(){
		return $this->mediaid;
	}
	
	public function setTipoMedia($tipo){
		$this->tipo_media = $tipo;
	}
	public function getTipoMedia(){
		return $this->tipo_media;
	}
	
	public function setUrlMedia($url){
		$this->url_media = $url;
	}
	public function getUrlMedia(){
		return $this->url_media;
	}
	
	public function setFonte($fonte){
		$this->fonte = $fonte;
	}
	public function getFonte(){
		return $this->fonte;
	}
	
	public function setNoticiaId($id){
		$this->noticiaid = $id;
	}
	public function getNoticiaId(){
		return $this->noticiaid;
	}
}
?>