<?php 
session_start();
use model\datamapper\UsuarioMapper;
use model\activerecord\UsuarioModel;
use model\datamapper\NoticiaMapper;
use model\activerecord\NoticiaModel;
use model\datamapper\Db;
use model\datamapper\Bcrypt;
//ini_set('display_errors', true);
require_once 'autoload.php';

$config = parse_ini_file(__DIR__ . DIRECTORY_SEPARATOR . 'db.ini');

$db = Db::getInstance($config);

$dmUsuario = new UsuarioMapper($db);

$model = new UsuarioModel();
$model->setDm($dmUsuario);
$model->setUserId($_SESSION['id']);


$dmNoticia = new NoticiaMapper($db);
$noticia = new NoticiaModel();
$noticia->setDm($dmNoticia);
$noticia->setUserId($_SESSION['id']);
$noticias = $noticia->sumarioNoticia();

if($_POST){
	
	if($_POST['btnSalvarFoto']){
		
		$fileName = $_FILES["fotoperfil"]["name"]; 
		$fileTmpLoc = $_FILES["fotoperfil"]["tmp_name"];
		$data = date( 'YmdHis' );
		// Path and file name
		$pathAndName = "img/".$data.$fileName;
		// Run the move_uploaded_file() function here
		$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
		
		$model->setAvatar($data.$fileName);
		$retorno = $model->updateFoto();
		
		if($retorno == 1)
			$_SESSION['avatar'] = $data.$fileName;
		
		header( 'Location: perfil.php#tab_2-2' );
		exit();
	}
	elseif ($_POST['btnUsuarios']){
		
		if($_GET['acao'] == 'Update'){
			
			$model->setUserId($_GET['ID']);
			$model->setNome($_POST['uNomeCompleto']);
			$model->setEmail($_POST['uEmail']);
			$model->setUsername($_POST['uApelido']);
			if($_POST['uSenha'])
				$model->setPass(Bcrypt::hash($_POST['uSenha']));
			
			$status = $model->update();
			
			
		}
		elseif ($_GET['acao'] == 'Insert'){
			
			$model->setNome($_POST['uNomeCompleto']);
			$model->setEmail($_POST['uEmail']);
			$model->setUsername($_POST['uApelido']);
			$model->setPass(Bcrypt::hash($_POST['uSenha']));
			$model->novoUser();
		
		}
		
		header( 'Location: perfil.php#tab_1_4' );
		exit();
	}
	
}
else
	if ((int)$_GET['key'] AND $_GET['acao'] == 'deletar'){
	
		$model->setUserId($_GET['key']);
		$retorno = $model->delete();
		
		if($retorno){
			header('Location: perfil.php#tab_1_4');
			exit();
		}
}

$user = Array();

$user = $model->dados();
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.0.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Consulfarma | Perfil Usuário</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<?php 
	include 'menu.php';
	?>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<a class="btn blue" id="btn-acao">Save changes</a>
							<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Meu perfil <small>perfil do usuário</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="javascript:;">Perfil</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="perfil.php">Meu Perfil</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">
								Overview </a>
							</li>
							<li>
								<a href="#tab_1_3" data-toggle="tab">
								Conta </a>
							</li>
							<?php 
							if($_SESSION['categoria'] == 1){
							?>
							<li>
								<a href="#tab_1_4" data-toggle="tab">
								Configuracoes </a>
							</li>
							<?php 
							}
							?>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
												<img src="img/<?=$user->getAvatar()?>" class="img-responsive" alt=""/>
												
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-8 profile-info">
												<h1><?=$user->getNome()?></h1>
												
												<ul class="list-inline">
													<li>
														<i class="fa fa-user"></i> <?=$user->getUsername()?>
													</li>
												</ul>
											</div>
											<!--end col-md-8-->
											<div class="col-md-4">
												<div class="portlet sale-summary">
													<div class="portlet-title">
														<div class="caption">
															Sumário de notícias
														</div>
														<div class="tools">
															<a class="reload" href="javascript:;">
															</a>
														</div>
													</div>
													<div class="portlet-body">
														<ul class="list-unstyled">
															<li>
																<span class="sale-info">
																Notícias Ativas <i class="fa fa-img-up"></i>
																</span>
																<span class="sale-num">
																<?=$noticias['ativas']?></span>
															</li>
															<li>
																<span class="sale-info">
																Notícias Desativas <i class="fa fa-img-down"></i>
																</span>
																<span class="sale-num">
																<?=$noticias['desativas']?> </span>
															</li>
															<li>
																<span class="sale-info">
																Total </span>
																<span class="sale-num">
																<?=$noticias['ativas']+$noticias['desativas']?> </span>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<!--end col-md-4-->
										</div>
										<!--end row-->
									</div>
								</div>
							</div>
							<!--tab_1_2-->
							<div class="tab-pane" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1-1">
												<i class="fa fa-cog"></i> Perfil </a>
												<span class="after">
												</span>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2-2">
												<i class="fa fa-picture-o"></i> Mudar Avatar </a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3-3">
												<i class="fa fa-lock"></i> Mudar Senha </a>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											<div id="tab_1-1" class="tab-pane active">
												<form role="form" action="#">
													<div class="form-group">
														<label class="control-label">Nome Completo</label>
														<input type="text"  value="<?=$user->getNome()?>" placeholder="John Doe" id="sNome" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="control-label">Apelido</label>
														<input type="text" value="<?=$user->getUsername()?>" placeholder="Goku" id="sApelido" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="control-label">Email</label>
														<input type="text" value="<?=$user->getEmail()?>" placeholder="meuemail@consulfarma.com" id="sEmail" class="form-control"/>
													</div>
													<div class="margiv-top-10">
														<a href="javascript:;" class="btn green" id="btnSalvarConta">
														Salvar </a>
														<span class="status-conta help-block" style="color:red;"></span>
													</div>
												</form>
											</div>
											<div id="tab_2-2" class="tab-pane">
												
												<form action="perfil.php" role="form" method="post" enctype="multipart/form-data">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="img/<?=$user->getAvatar()?>" alt=""/>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Selecione image </span>
																<span class="fileinput-exists">
																Trocar </span>
																<input type="file" name="fotoperfil">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																Remover </a>
															</div>
														</div>
														<div class="clearfix margin-top-10">
															<span class="label label-danger">
															Nota! </span>
															 <span>
															 Soporta Firefox, Chrome, Opera, Safari e Internet Explorer 10 apenas </span>
														</div>
													</div>
													<div class="margin-top-10">
														<input type="submit" class="btn green" value="Enviar" name="btnSalvarFoto">
														
													</div>
												</form>
											</div>
											<div id="tab_3-3" class="tab-pane">
												<form action="#">
													<div class="form-group">
														<label class="control-label">Senha Atual</label>
														<input type="password" id="senhaatual" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="control-label">Nova Senha</label>
														<input type="password" id="novasenha" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="control-label">Repita Nova Senha</label>
														<input type="password" id="novasenha2" class="form-control"/>
													</div>
													<div class="margin-top-10">
														<a href="javascript:;" class="btn green" id="btnTrocarSenha">
														Trocar Senha </a>
														<span class="status-senha help-block" style="color:red"></span>
													</div>
												</form>
											</div>
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<?php 
							
							if($_SESSION['categoria'] == 1){
							
								$model->setUserId(null);
								$users = $model->dados();
											
							?>
							<div class="tab-pane" id="tab_1_4">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1-1-1">
												<i class="fa fa-user"></i> Novo Usuário </a>
												<span class="after">
												</span>
											</li>
										</ul>
									</div>
									<?php 
									$acao = 'acao=Insert';
									$idform = 'form-novousuario';
									if($_GET['key']){
										foreach($users as $idUser => $valueUser){
											
											if($_GET['key'] === $valueUser->getUserId()){
												$apelido = $valueUser->getUsername();
												$nome = $valueUser->getNome();
												$email = $valueUser->getEmail();
												$jm = 'selected="selected"';
												$acao = 'acao=Update&ID='.$valueUser->getUserId().'#tab_1_4';
												$idform = 'form-alterarusuario';
												break;
											}
										}
										
										
									}
									?>
									<div class="col-md-9">
										<div class="tab-content">
											<div id="tab_1-1-1" class="tab-pane active">
												<form role="form" action="perfil.php?<?=$acao?>" method="post" id="<?=$idform?>">
													<div class="form-group">
														<label class="control-label">Nome Completo</label>
														<input type="text" placeholder="John Doe" class="form-control" name="uNomeCompleto" value="<?=$nome?>"/>
													</div>
													<div class="form-group">
														<label class="control-label">Apelido</label>
														<input type="text" placeholder="Goku" name="uApelido" class="form-control" value="<?=$apelido?>"/>
													</div>
													<div class="form-group">
														<label class="control-label">Email</label>
														<input type="text" placeholder="meuemail@consulfarma.com" name="uEmail" class="form-control" value="<?=$email?>"/>
													</div>
													<div class="form-group">
														<label class="control-label">Senha</label>
														<input type="password" placeholder="" name="uSenha" class="form-control"/>
													</div>
													<div class="form-group" style="display: none;">
														<label class="control-label">Categória</label>
														<select class="form-control" name="uCategoria">
															<option value="0">selecione</option>
															<option value="1" <?=$jm?>>Jornalista</option>
															<option value="2">Administrador</option>
														</select>
													</div>
													<div class="margiv-top-10">
														<input type="submit" name="btnUsuarios" value="Salvar" class="btn green">
														
													</div>
												</form>
												</br>
												<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">
													Usuários </a>
												</li>
												
											</ul>
											
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
														<tr>
															<th>
																<i class="fa fa-user"></i> Nome
															</th>
															<th class="hidden-xs">
																<i class="fa fa-question"></i> Apelido
															</th>
															<th>
																<i class="fa fa-envelope-o"></i> Email
															</th>
															<th>
															</th>
														</tr>
														</thead>
														<tbody>
														<?php 
														foreach($users as $userid => $values){
																
															if($_SESSION['id'] != $values->getUserId()){
																
														?>
														<tr>
															<td>
																<a href="#">
																<?=$values->getNome()?> </a>
															</td>
															<td class="hidden-xs">
																 <?=$values->getUsername()?>
															</td>
															<td>
																<?=$values->getEmail()?>
															</td>
															<td>
																<a class="btn default btn-xs green-stripe" href="perfil.php?key=<?=$values->getUserId()?>#tab_1_4">
																Editar </a>
																<a class="btn default btn-xs red-stripe" href="javascript:;" onclick="Perfil.modal(1,<?=$values->getUserId()?>);">
																Deletar </a>
															</td>
														</tr>
														<?php 
															}
														}
														?>
														</tbody>
														</table>
													</div>
												</div>
												
												<!--tab-pane-->
											</div>
										</div>
											</div>	
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<?php 
							}
							?>
						</div>
					</div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 Consulfarma.
	</div>
	<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="js/perfil.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Perfil.init();

	//$( '#portlet-config' ).modal( 'show' );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>