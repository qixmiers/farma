<?php
session_start();

use model\datamapper\Token;
use model\activerecord\TokenModel;
use model\datamapper\NoticiaMapper;
use model\activerecord\NoticiaModel;
use model\datamapper\Db;
use model\datamapper\Bcrypt;

ini_set('display_errors', true);

if($_SESSION['token']){

	require_once '../autoload.php';
	
	$config = parse_ini_file('../db.ini');
	$db = Db::getInstance($config);
	$dmToken = new Token($db);
	$token = new TokenModel();
	$token->setId($_SESSION['id']);
	$auxtoken = $token->getToken($dmToken);
	
	if($auxtoken['token'] === $_SESSION['token']){
		
		$dmNoticia = new NoticiaMapper($db);
		
		$model = new NoticiaModel();
		$model->setDm($dmNoticia);
		
		$model->setUserId($_SESSION['id']);
		$model->setTitulo($_POST['nTitulo']);
		$model->setSubTitulo($_POST['nSubtitulo']);
		$model->setDataInserido($_POST['nInicio']);
		$model->setDataExpira($_POST['nTermino']);
		$model->setCategoriaId($_POST['nCategoria']);
		$model->setTipoNoticia($_POST['nLayout']);
		$model->setDescricao($_POST['nDescricao']);
		$model->setUrlAvatar($_POST['nCapa']);
		
		$nId = $model->novaNoticia();
		
		echo $nId;
		exit();
	}else{
		echo 'session:0';
		exit();
	}
}
else{
	echo 'session:0';
	exit();
}
?>