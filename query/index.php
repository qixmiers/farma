<?php
session_start();

use model\datamapper\Token;
use model\activerecord\TokenModel;
use model\datamapper\NoticiaMapper;
use model\activerecord\NoticiaModel;
use model\datamapper\Db;
use model\datamapper\Bcrypt;
ini_set('display_errors', false);
if($_SESSION['token']){

		require_once '../autoload.php';

		$config = parse_ini_file('../db.ini');
		$db = Db::getInstance($config);
		$dmToken = new Token($db);
		$token = new TokenModel();
		$token->setId($_SESSION['id']);
		$auxtoken = $token->getToken($dmToken);

		if($auxtoken['token'] == $_SESSION['token']){
			$dmNoticia = new NoticiaMapper($db);
			$model = new NoticiaModel();
			$model->setDm($dmNoticia);
			$categorias = Array();
			$noticias = Array();

			$categorias = $model->getAllCategoria();
			if($_POST['sAction'] != 'filter')
				$noticias = $model->dados();
			elseif($_POST['sAction'] == 'filter'){

				if($_POST['data_inicio'])
					$datainicio = $model->getdata($_POST['data_inicio']);
				if($_POST['data_termino'])
					$datatermino = $model->getdata($_POST['data_termino']);

				$where = Array(
					'data_inicio' => $datainicio,
					'data_termino' => $datatermino,
					'sTitulo' => $_POST['sTitulo'],
					'sSubtitulo' => $_POST['sSubtitulo'],
					'noticia_status' => $_POST['noticia_status'],
					'nCategoria' => $_POST['nCategoria'],
				);

				$noticias = $model->dadosFilter($where);
			}

			$cont = 0;
			$sEcho = intval($_REQUEST['sEcho']);
			$records = array();
			$records["aaData"] = array();


			$status_list = array(
				array("success" => "Ativo"),
			    array("danger" => "Desativado")
			 );

			foreach($noticias as $id => $noticia){

				$key = 0;
				$dataExpira = '/sem termino ';

				if($noticia->getDataExpira() != '0000-00-00 00:00:00')
					$dataExpira = ' até '. $model->getdataBR($noticia->getDataExpira());

				if($model->getdataBR($noticia->getDataExpira()) < date( 'd/m/Y' ) AND $noticia->getDataExpira() != '0000-00-00 00:00:00')
					$key = 1;

				$acao = '<a href="nova-noticia.php?ID='.$noticia->getNoticiaId().'" class="btn btn-xs default"><i class="fa fa-search"></i> Editar</a>
					</br>';
				if($_SESSION['categoria'] == 1){
					$acao .= '<a href="javascript:;" onclick="Index.deletar('.$noticia->getNoticiaId().');" class="btn btn-xs red"><i class="fa fa-times"></i> Deletar</a>';
				}
				$records["aaData"][] = array(
				    $model->getdataBR($noticia->getDataInserido()).$dataExpira,
				    $noticia->getTitulo(),
				    $noticia->getSubTitulo(),
				    '<span class="label label-sm label-'.(key($status_list[$key])).'">'.(current($status_list[$key])).'</span>',
					$categorias[$noticia->getCategoriaId()],
				    $acao
				  );
			  	$cont++;
			 }

			if (isset($_REQUEST["sAction"]) && $_REQUEST["sAction"] == "group_action") {
			    $records["sStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
			    $records["sMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
			  }

			 $records["sEcho"] = $sEcho;
			 $records["iTotalRecords"] = $cont;
			 $records["iTotalDisplayRecords"] = $cont;

			 echo json_encode($records);
			 exit();


	}else{
		echo json_encode( 'session:0' );
	}
}
?>