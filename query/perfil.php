<?php
session_start();

use model\datamapper\Token;
use model\activerecord\TokenModel;
use model\datamapper\UsuarioMapper;
use model\activerecord\UsuarioModel;
use model\datamapper\Db;
use model\datamapper\Bcrypt;

//ini_set('display_errors', true);

if($_SESSION['token']){
	 
	require_once '../autoload.php';
	
	$config = parse_ini_file('../db.ini');
	$db = Db::getInstance($config);
	$dmToken = new Token($db);
	$token = new TokenModel();
	$token->setId($_SESSION['id']);
	$auxtoken = $token->getToken($dmToken);
	
	if($auxtoken['token'] === $_SESSION['token']){

		if($_GET['action'] == 'perfil'){
			
			$dmUsuario = new UsuarioMapper($db);
			
			$model = new UsuarioModel();
			$model->setDm($dmUsuario);
			
			$model->setUserId($_SESSION['id']);
		
			if($_GET['metodo'] == 'alterar_conta'){
				
				$model->setNome($_GET['sNome']);
				$model->setEmail($_GET['sEmail']);
				$model->setUsername($_GET['sApelido']);
				$status = $model->update();
				if($status == 1)
					$_SESSION['nome'] = $_GET['sNome'];
				
				echo json_encode($status);
				exit();
			}
			elseif ($_GET['metodo'] == 'alterar_senha'){
				
				$user = $model->dados();
				
				if(Bcrypt::check($_GET['sAtual'], $user->getHash())){
					
					$model->setPass(Bcrypt::hash($_GET['sNova']));
					$status = $model->updateSenha();
					
					echo json_encode($status);
					exit();
				}
				
				echo json_encode('Senha atual errada');
				exit();
				
			}
			
		}
		
	}
	
	echo json_encode('0');
	exit();
}
echo json_encode('0');
?>